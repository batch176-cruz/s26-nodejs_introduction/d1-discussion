let http = require("http");

http.createServer(function(request,response){

	response.writeHead(200,{'Content-Type':'text/plain'});
	response.end("Hello from our Server in sample.js!");

}).listen(8000);

console.log("Server is running on localhost:8000");